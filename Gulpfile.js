var gulp     = require('gulp'),
  uglify     = require('gulp-uglify'),
  cleanCSS     = require('gulp-clean-css'),
  concat     = require('gulp-concat'),
  ngTemplate = require('gulp-angular-templatecache'),
  baseDest   = './public/dist/',
  pkg        = require('./package.json');


function onWarning(error) { handleError.call(this, 'error', error);}

gulp.task('css', function() {
  return gulp.src(['./public/css/*.css'])
    .pipe(concat('style.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest(baseDest + 'css/'));
});

gulp.task('script', function() {
  return gulp.src([
    './public/modules/main.js',
    './public/modules/*/*.js',
    './public/modules/**/*.js'
  ])
    .pipe(concat('script.js'))
    .pipe(uglify())
    .pipe(gulp.dest(baseDest + 'js/'));
});

gulp.task('template', function() {
  return gulp.src('./public/modules/**/*.html')
    .pipe(ngTemplate({
      module: pkg.name + '.templates'
    }))
    .pipe(gulp.dest(baseDest + 'js/'))
});

gulp.task('default', ['css', 'script', 'template']);

