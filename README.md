# Employee Network Application


User should have installed git and node.

2. User should have gulp and bower installed in global npm packages.

3. If user is not having gulp and bower install please install it using following commands.

  i. npm i -g bower

  ii. npm i -g gulp

# What is it?

  Employee Network Application is a Single Page Application(SPA) created on top of Angular JS.

  To run this example, follow this walkthrough:

  1. Install node <https://nodejs.org/en/>

  2. Open terminal.

  3. Go to folder location in terminal.

  4. run ```npm install```.

  5. go inside public folder and run ```bower install```.

  6. go back to root of project and run ```gulp``` to generate output.

  7. Run ```npm start```.

  8. Open browser and hit the URL <http://localhost:3000>

  9. **Employee Network App** is now up and running on browser.

