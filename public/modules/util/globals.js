/*
* Global Utilities
* Contains generic methods
* */

 (function() {
    'use strict';

    window.globals = {
        getNumber: function(num) {
            return new Array(num || 0);
        }
    }

})();
