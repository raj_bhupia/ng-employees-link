/*
 * Main Modules
 * Contains angular module dependencies
 * */

(function() {
  'use strict';

  angular.module('employeeApp', [
    'employeeApp.config',
    'employeeApp.app',
    'employeeApp.home',
    'employeeApp.templates'
  ]);

})();

