/*
 * Application Bootstrap
 * angular application starting point. App will start on DOM is ready
 * */


(function() {
    'use strict';

    function loadApp() {
        try {
            angular.bootstrap(document, ['employeeApp']);
        } catch (err) {
            console.log('error: ' + err);
        }
    }
    // Listen to device ready
    angular.element(document).ready(function() {
        loadApp()
    });

})();