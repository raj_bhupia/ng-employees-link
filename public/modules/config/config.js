/*
 * Configuration file
 * Contains app global configurations
 * */

(function() {
  'use strict';

  angular.module('employeeApp.config', [])
    .constant("CONFIG", {
      "appName": "employeeApp",
      "baseAPI": "https://example.com/",
      "defaultRoute": "/home"
    });

})();
