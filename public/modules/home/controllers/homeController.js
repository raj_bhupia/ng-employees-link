(function () {
  'use strict';

  angular.module('employeeApp.home').controller('HomeController', ['$scope', 'HomeService',
    function ($scope, HomeService) {
    	$scope.empNetwork = HomeService.getEmpNetwork()
    }])

})();