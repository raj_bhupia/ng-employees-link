(function() {
    'use strict';

    angular.module("employeeApp.home")
        .directive('eNetwork', ['$document', function($document) {
            return {
                restrict: 'E',
                scope: {
                    data: '=info'
                },
                template: '<canvas id="myCanvas" width="800" height="600"></canvas>',
                link: function(scope, element, attr) {
                    var canvas,
                        offsetX,
                        offsetY,
                        cw,
                        ch,
                        shapeList = [],
                        empNetwork = scope.data;

                    function onDocLoaded() {
                        canvas = document.getElementById('myCanvas');
                        reOffset();
                        window.onscroll = function(e) {
                            reOffset();
                        }
                        window.onresize = function(e) {
                            reOffset();
                        }
                        canvas.onmousemove = function(e) {
                            handleMouseMove(e);
                        }
                        drawMap()
                    }

                    function reOffset() {
                        var BB = canvas.getBoundingClientRect();
                        offsetX = BB.left;
                        offsetY = BB.top;

                        cw = canvas.width;
                        ch = canvas.height;
                    }

                    function handleMouseMove(e) {
                        // tell the browser we're handling this event
                        e.preventDefault();
                        e.stopPropagation();

                        var mouseX = parseInt(e.clientX - offsetX),
                            mouseY = parseInt(e.clientY - offsetY);

                        var ctx = canvas.getContext('2d');
                        ctx.clearRect(0, 0, cw, ch);
                        drawMap(mouseX, mouseY)
                    }

                    function drawMap(mouseX, mouseY) {
                        var i;

                        for (i = 0; i < empNetwork.length; i++) {
                            shapeList[i] = new circle_t(i);
                            shapeList[i].draw(canvas, null, mouseX, mouseY);
                        }

                        for (i = 0; i < empNetwork.length - 1; i++) {
                            var linkedTo = empNetwork[i].linkedTo;
                            for (var j = 0; j < linkedTo.length; j++) {
                                var idx = empNetwork.findIndex(function(val, idx) {
                                    if (val.eid === linkedTo[j]) {
                                        return true
                                    }
                                })
                                draw_line2(shapeList[i].origX, shapeList[i].origY, shapeList[i].radius, shapeList[idx].origX, shapeList[idx].origY, shapeList[idx].radius);
                            }
                        }
                    }

                    var shape_t = function(x, y) {
                        this.origX = (x == undefined ? 0 : x);
                        this.origY = (y == undefined ? 0 : y);
                    }
                    shape_t.prototype = {
                        origX: 0,
                        origY: 0,
                        typeString: 'shape',
                        setPos: function(x, y) {
                            this.x = x;
                            this.y = y;
                        },
                        setType: function(typeString) {
                            this.typeString = typeString;
                        },
                        toString: function() {
                            return this.typeString + " - " + this.origX + "," + this.origY;
                        },
                        draw: function(canElem) {},
                    };

                    function getRandomNumber() {
                        var random = Math.random();
                        return (random > .6) ? random * 10 : getRandomNumber()
                    }

                    function circle_t(idx) {
                        if (idx === 'undefined')
                            return;

                        empNetwork[idx].x = empNetwork[idx].x || getRandomNumber()
                        empNetwork[idx].y = empNetwork[idx].y || getRandomNumber()

                        this.origX = ((idx + 1) * empNetwork[idx].x * 7);
                        this.origY = ((idx + 1) * empNetwork[idx].y * 5);
                        this.radius = 30;
                        this.hoverText = (empNetwork[idx].eid + ' / ' + empNetwork[idx].dept);
                        this.fillText = empNetwork[idx].name;
                        this.linkedTo = empNetwork[idx].linkedTo;

                        this.setType("circle");
                    }
                    circle_t.prototype = new shape_t();
                    circle_t.prototype.constructor = circle_t;
                    circle_t.prototype.draw = function(canElem, color, mouseX, mouseY) {
                        var ctx = canElem.getContext('2d');
                        var col = 'black';
                        if (color != undefined)
                            col = color;
                        drawCircle.call(this, this.origX, this.origY, this.radius, ctx, col, mouseX, mouseY);
                    }

                    circle_t.prototype.setRadius = function(radius) {
                        if (radius != undefined)
                            this.radius = radius;
                    }

                    function drawCircle(x, y, radius, ctx, col, mouseX, mouseY) {
                        ctx.save();
                        if (col == undefined)
                            col = 'black';
                        ctx.strokeStyle = col;
                        ctx.lineWidth = 1;
                        ctx.beginPath();
                        ctx.arc(x, y, radius, (Math.PI / 180) * 0, (Math.PI / 180) * 360, false);
                        ctx.stroke();
                        ctx.fillText(this.fillText, x - radius + 17, y + 2);
                        ctx.closePath();

                        if (ctx.isPointInPath(mouseX, mouseY)) {
                            ctx.fillText(this.hoverText, x, y - radius - 2);
                        }

                        ctx.restore();
                    }

                    // define a vec2 class to make vector maths easier (simpler to read)
                    function vec2(x, y) {
                        this.length = function() {
                            return Math.sqrt((this.x * this.x) + (this.y * this.y));
                        }
                        this.normalize = function() {
                            var scale = this.length();
                            this.x /= scale;
                            this.y /= scale;
                        }
                        this.x = x;
                        this.y = y;
                    }

                    function draw_line2(center1_x, center1_y, radius1, center2_x, center2_y, radius2) {
                        var betweenVec = new vec2(center2_x - center1_x, center2_y - center1_y);
                        betweenVec.normalize();

                        var p1x = center1_x + (radius1 * betweenVec.x);
                        var p1y = center1_y + (radius1 * betweenVec.y);

                        var p2x = center2_x - (radius2 * betweenVec.x);
                        var p2y = center2_y - (radius2 * betweenVec.y);

                        var canvas = document.getElementById('myCanvas');
                        var context = canvas.getContext('2d');
                        context.beginPath();
                        context.moveTo(p1x, p1y);
                        context.lineTo(p2x, p2y);
                        context.stroke();
                    }

                    onDocLoaded()

                }
            }
        }])

})();