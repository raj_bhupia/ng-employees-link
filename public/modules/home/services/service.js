(function() {
  'use strict';

  angular.module('employeeApp.home')
    .service('HomeService', ['$http', function($http) {
      // define web services route here
      this.getEmpNetwork = function(params) {
        return [{
          name: 'Lisa',
          eid: 2006,
          dept: "Retail",
          linkedTo: [2001, 2005]
        }, {
          name: 'Susan',
          eid: 2005,
          dept: "Retail",
          linkedTo: []
        }, {
          name: 'David',
          eid: 2001,
          dept: "Markets",
          linkedTo: [2003, 2004, 2007]
        }, {
          name: 'Raghu',
          eid: 2003,
          dept: "Markets",
          linkedTo: [2002]
        }, {
          name: 'Ana',
          eid: 2004,
          dept: "KYC",
          linkedTo: []
        }, {
          name: 'Scott',
          eid: 2007,
          dept: "Markets",
          linkedTo: [2008]
        }, {
          name: 'Ruben',
          eid: 2002,
          dept: "Finance",
          linkedTo: []
        }, {
          name: 'Mike',
          eid: 2008,
          dept: "AML",
          linkedTo: [2009]
        }, {
          name: 'Kris',
          eid: 2009,
          dept: "AML",
          linkedTo: []
        }]
        // call webservice like this to fetch the data from server
        //return $http.post('/login', params);
      }
    }]);

})();
