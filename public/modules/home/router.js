(function() {
    'use strict';

    angular.module("employeeApp.home")
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('home', {
                    parent: 'app',
                    url: '/home',
                    views: {
                        "content": {
                            templateUrl: 'modules/home/templates/home.html',
                            controller: 'HomeController'
                        }
                    }
                });
        }])

})();