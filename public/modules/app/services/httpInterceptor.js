(function() {
  'use strict';

  angular.module('employeeApp.app')
    .factory('HttpInterceptor',['$rootScope', '$q', '$timeout', 'CONFIG', function($rootScope, $q, $timeout, CONFIG) {
      return {
        // Intercept request before sending it to server
        request: function(req) {
          var systemKeys = ['js', 'json', 'html'];
          var type = req.url.split('.');

          if(systemKeys.indexOf(type[type.length-1]) == -1){
            req.url = CONFIG.baseAPI + req.url;
          }
          req.timeout = 30000;
          return req || $q.when(req);
        },
        response: function(response) {
          // intercept response from server
          return response;
        },
        responseError: function(error) {
          // handling error
          return $q.reject(error);
        }
      };
    }])

})();
