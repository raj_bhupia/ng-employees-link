(function () {
    'use strict';

    angular.module('employeeApp.app').service('AuthService', ['$rootScope','$state',
        function ($rootScope, $state) {
        return {
            session: function () { // session created after login (value is available in sessionStorage)
                return ("User Detail" in localStorage);
            },
            getLoginDetails: function () { // function for getting the user detail stored in sessionStorage
                return JSON.parse(atob(localStorage.getItem("User Detail"))) || {};
            },
            logout: function () {
                // sessionStorage.clear();
                localStorage.removeItem('User Detail');
            },
            setLoginDetails: function (data) { // function for setting the user detail in sessionStorage
                localStorage.setItem("User Detail", btoa(JSON.stringify(data)));
            }
        }
    }]);

})();