(function () {
    'use strict';

    angular.module('employeeApp.app')

        .config(['$httpProvider', function ($httpProvider) {
            // adding interceptor in httpProvider
            $httpProvider.interceptors.push("HttpInterceptor");
        }])

        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'CONFIG',
            function ($stateProvider, $urlRouterProvider, $locationProvider, CONFIG) {
                // application start point
                $stateProvider
                    .state('app', {
                        url: '',
                        abstract: true,
                        templateUrl: 'modules/app/templates/base.html',
                        controller: 'AppController'
                    });

                $urlRouterProvider.otherwise(CONFIG.defaultRoute);

                // use the HTML5 History API
                $locationProvider.html5Mode(true);
            }])

        // run method runs immediately after the application start
        .run(['$rootScope', '$location', '$timeout', 'AuthService', 'CONFIG',
            function ($rootScope, $location, $timeout, AuthService, CONFIG) {
                $rootScope.$on('$stateChangeStart', function () {
                    // used to write state change logic wherever needed
                });
            }])

})();
