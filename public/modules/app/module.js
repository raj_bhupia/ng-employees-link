(function() {
    'use strict';

    angular.module('employeeApp.app', [
        'ui.router'
    ]);

    // templates module declaration, will used with gulp-angular-templatecache
    angular.module('employeeApp.templates', [])

})();
